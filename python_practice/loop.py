#Loop practice
#date: 30-nov-2017
#book: introduction to computation and programming using python
#enter 10 integers and find largest odd number. if not enter any
#odd number then print accordingly
n = 0
num_list = []
while ( n != 10 ):
    i = input("Enter the number: ")
    if ( i % 2 == 1):
        num_list.append(i)
    n += 1

if (len(num_list) == 0):
    print "all number entered by user are even number"
else:
    print max(num_list)


