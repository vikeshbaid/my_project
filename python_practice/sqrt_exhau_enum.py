#one more looping program

x = 25
epsilion = 0.01
step = epsilion ** 2
numGuesses = 0
ans = 0.0
while abs(ans**2 - x) >= epsilion and ans <= x:
    ans += step
    numGuesses += 1
print 'numGuesses =', numGuesses
if abs(ans**2 - x) >= epsilion:
    print "Failed on square root of", x
else:
    print ans, 'is close to square root of', x
